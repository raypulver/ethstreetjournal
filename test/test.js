'use strict';

const easySolc = require('../src/lib/easy-solc');
const esj = require('../src/lib/ethstreetjournal');
const fs = require('fs');
const path = require('path');
const rpcCall = require('kool-makerpccall');
const rpcAddress = 'http://localhost:8545';
const Web3 = require('web3');
const web3 = new Web3(rpcAddress);
const { expect } = require('chai');
const call = (method, params = []) => rpcCall(rpcAddress, method, params);

let bytecode;
try {
  bytecode = easySolc('ETHStreetJournal', fs.readFileSync(path.join(__dirname, '..', 'src', 'contracts', 'ETHStreetJournal.sol'), 'utf8')).bytecode;
} catch (e) {
  console.log(e.errors);
}

describe('ethstreetjournal', () => {
  it('should publish an article and reread it', async () => {
    const [ from ] = await call('eth_accounts');
    const { contractAddress } = await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
      from,
      data: bytecode,
      gas: 6e6,
      gasPrice: '0x1'
    }]) ]);
    const article = {
      title: 'title',
      description: 'my description',
      flags: {
        useMarkdown: true,
        isPrivate: true,
        allowComments: true
      },
      payload: 'woop'
    };
    const tx = await esj.publishArticle(web3, contractAddress, article, {
      from,
      gas: 6e6
    });
    const { article: decoded }  = await esj.getArticleByUserAndNonce(web3, contractAddress, from, 0);
    expect(decoded).to.eql(article);
  });
  it('should publish an article to a permalink', async () => {
    const [ from ] = await call('eth_accounts');
    const { contractAddress } = await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
      from,
      data: bytecode,
      gas: 6e6,
      gasPrice: '0x1'
    }]) ]);
    const article = {
      title: 'title',
      description: 'my description',
      flags: {
        useMarkdown: true,
        isPrivate: true,
        allowComments: true
      },
      permalink: 'yeet',
      payload: 'woop'
    };
    const tx = await esj.publishArticle(web3, contractAddress, article, {
      from,
      gas: 6e6
    });
    const output  = await esj.getArticleByPermalink(web3, contractAddress, 'yeet');
    expect(output.permalink).to.eql(article.permalink);
  });
  it('should publish and read comments', async () => {
    const [ from ] = await call('eth_accounts');
    const { contractAddress } = await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
      from,
      data: bytecode,
      gas: 6e6,
      gasPrice: '0x1'
    }]) ]);
    const article = {
      title: 'title',
      description: 'my description',
      flags: {
        useMarkdown: true,
        isPrivate: true,
        allowComments: true
      },
      permalink: 'yeet',
      payload: 'woop'
    };
    const tx = await esj.publishArticle(web3, contractAddress, article, {
      from,
      gas: 6e6
    });
    const articleHash = esj.computeArticleHash(from, 0);
    await esj.publishComment(web3, contractAddress, {
      comment: 'woopdoop',
      articleHash
    }, {
      from,
      gas: 6e6
    });
    const output = await esj.getComment(web3, contractAddress, articleHash, 0);
    expect(output.comment).to.eql('woopdoop')
  });
});
