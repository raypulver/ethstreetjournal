'use strict';

const rlp = require('rlp');
const gzip = require('gzip-js');
const {
  soliditySha3
} = require('web3-utils');
const {
  stripHexPrefix,
  addHexPrefix,
  fromAscii,
  toAscii,
  toHex,
  toBuffer,
  bufferToHex
} = require('ethereumjs-util');
const abi = require('web3-eth-abi');
const {
  decodeParameters: web3DecodeParametersUnbound,
  encodeFunctionCall: web3EncodeFunctionCallUnbound
} = abi;

const web3DecodeParameters = web3DecodeParametersUnbound.bind(abi);
const encodeFunctionCall = web3EncodeFunctionCallUnbound.bind(abi);

const offsets = {
  comments: '0x0',
  userData: '0x1',
  idToEntry: '0x2',
  additionalPages: '0x3',
  articles: '0x4'
};

const decodeParameters = (abi, input) => {
  const decoded = web3DecodeParameters(abi, input);
  return [].slice.call(Object.assign(decoded, {
    length: decoded.__length__
  }));
};

const PUBLISH_ARTICLE_SIGNATURE = soliditySha3('publishArticle(string,bytes)').substr(0, 10);
const PUBLISH_COMMENT_SIGNATURE = soliditySha3('publishComment(bytes32,bytes)').substr(0, 10);
const flags = require('./flags');

const bufferToByteArray = (buffer) => [].slice.call(buffer);

const zip = (hex) => bufferToHex(toBuffer(gzip.zip(bufferToByteArray(hex), { level: 3 })));

const unzip = (hex) => toBuffer(gzip.unzip(bufferToByteArray(toBuffer(addHexPrefix(hex))), { level: 3 }))

const unpackArticle = (input) => {
  const [
    titleHex,
    descriptionHex,
    flagsHex,
    payloadHex
  ] = rlp.decode(unzip(input.substr(2))).map((v) => bufferToHex(v));
  const flagsDecoded = flags.decodeFlags(flagsHex);
  const [ title, description, payload ] = [ titleHex, descriptionHex, payloadHex ].map((v) => toAscii(v));
  return {
    title,
    description,
    payload,
    flags: flagsDecoded
  };
};

let id = 0;

const publishArticle = (web3, contractAddress, {
  title,
  permalink = '',
  description,
  flags: {
    useMarkdown,
    isPrivate,
    allowComments
  },
  payload
}, txParams) => {
  const encoded = bufferToHex(zip(rlp.encode([ toBuffer(fromAscii(title)), toBuffer(fromAscii(description)), toBuffer(flags.encodeFlags(useMarkdown, isPrivate, allowComments)), toBuffer(fromAscii(payload)) ])));
  const data = encodeFunctionCall({
    name: 'publishArticle',
    inputs: [{
      type: 'string',
      name: 'permalink'
    }, {
      type: 'bytes',
      name: 'payload'
    }]
  }, [ permalink, encoded ]);
  return new Promise((resolve, reject) => {
    web3.currentProvider.send({
      id: id++,
      jsonrpc: '2.0',
      method: 'eth_sendTransaction',
      params: [ Object.assign({}, txParams, {
        data,
        to: contractAddress
      }) ]
    }, (err, result) => {
      if (err) return reject(err);
      resolve(result.result);
    });
  });
};

const unpackComment = (input) => toAscii(bufferToHex(unzip(input)));

const publishComment = (web3, contractAddress, {
  comment,
  articleHash
}, txParams) => {
  const encoded = bufferToHex(zip(toBuffer(fromAscii(comment))));
  const data = encodeFunctionCall({
    name: 'publishComment',
    inputs: [{
      type: 'bytes32',
      name: 'articleHash'
    }, {
      type: 'bytes',
      name: 'payload'
    }]
  }, [ articleHash, encoded ]);
  return new Promise((resolve, reject) => {
    web3.currentProvider.send({
      id: id++,
      jsonrpc: '2.0',
      method: 'eth_sendTransaction',
      params: [ Object.assign({}, txParams, {
        data,
        to: contractAddress
      }) ]
    }, (err, result) => {
      if (err) return reject(err);
      resolve(result.result);
    });
  });
};

const getArticleMetadataByHash = async (web3, contractAddress, articleHash) => {
  const entryRecord = await getArticleEntryRecord(web3, contractAddress, articleHash);
  const record = await getArticleRecord(web3, contractAddress, entryRecord.index);
  return {
    entryRecord,
    record
  };
};


const getArticleByHash = async (web3, contractAddress, articleHash) => fetchCompleteArticle(web3, contractAddress, await getArticleMetadataByHash(web3, contractAddress, articleHash));

const computeArticleHash = (address, nonce) => soliditySha3({
  t: 'address',
  v: address
}, {
  t: 'uint32',
  v: nonce
});
  

const getArticleByUserAndNonce = async (web3, contractAddress, address, nonce) => {
  const articleHash = computeArticleHash(address, nonce);
  return getArticleByHash(web3, contractAddress, articleHash);
};

const getArticleByIndex = async (web3, contractAddress, index) => {
  const record = await getArticleRecord(web3, contractAddress, index);
  const {
    indexOfArticleByUser,
    owner
  } = record;
  const articleHash = computeArticleHash(owner, indexOfArticleByUser);
  const entryRecord = await getArticleEntryRecord(web3, contractAddress, articleHash);
  return fetchCompleteArticle(web3, contractAddress, {
    record,
    entryRecord
  });
};

const getArticleByPermalink = async (web3, contractAddress, permalink) => {
  const index = await web3.eth.call({
    to: contractAddress,
    data: encodeFunctionCall({
      name: 'permaLinkToIndex',
      inputs: [{
        name: 'permalink',
        type: 'bytes32'
      }]
    }, [ soliditySha3({
      t: 'string',
      v: permalink
    }) ])
  });
  return getArticleByIndex(web3, contractAddress, index);
};

const getArticleEntryRecord = async (web3, contractAddress, articleHash) => {
  const [
    index,
    downvotes,
    upvotes,
    commentCount,
    permalink
  ] = decodeParameters(['uint256', 'uint32', 'uint32', 'uint32', 'string'], await web3.eth.call({
    to: contractAddress,
    data: encodeFunctionCall({
      name: 'idToEntry',
      inputs: [{
        name: 'articleHash',
        type: 'bytes32'
      }]
    }, [ articleHash ])
  }));
  return {
    index,
    downvotes,
    upvotes,
    commentCount,
    permalink
  };
};

const getArticleRecord = async (web3, contractAddress, index) => {
  const [
    blockNumber,
    indexOfArticleByUser,
    globalArticleNumber,
    owner
  ] = decodeParameters(['uint32', 'uint32', 'uint32', 'address'], await web3.eth.call({
    to: contractAddress,
    data: encodeFunctionCall({
      name: 'articles',
      inputs: [{
        name: 'index',
        type: 'uint256'
      }]
    }, [ index ])
  }));
  return {
    blockNumber,
    indexOfArticleByUser,
    globalArticleNumber,
    owner
  };
};

const getUserStats = async (web3, contractAddress, address) => {
  const [
    totalArticles,
    username
  ] = decodeParameters(['uint256', 'string'], await web3.eth.call({
    to: contractAddress,
    data: encodeFunctionCall({
      name: 'userData',
      inputs: [{
        name: 'user',
        type: 'address'
      }]
    }, [ address ])
  }));
  return {
    totalArticles,
    username
  };
};

const getCommentRecord = async (web3, contractAddress, articleHash, index) => {
  const [
    blockNumber,
    upvotes,
    downvotes,
    owner
  ] = decodeParameters(['uint32', 'uint32', 'uint32', 'address'], await web3.eth.call({
    to: contractAddress,
    data: encodeFunctionCall({
      name: 'comments',
      inputs: [{
        type: 'bytes32',
        name: 'hash'
      }]
    }, [ soliditySha3({
      t: 'bytes32',
      v: articleHash
    }, {
      t: 'uint32',
      v: index
    }) ])
  }));
  return {
    blockNumber,
    upvotes,
    downvotes,
    owner
  };
};

const indexOfArticleForBlockForUser = async (web3, contractAddress, metadata) => {
  const {
    blockNumber,
    indexOfArticleByUser
  } = metadata.record;
  let current = metadata;
  let previous = Number(metadata.record.indexOfArticleByUser) - 1;
  while (previous > 0) {
    const previousMetadata = await fetchArticleMetadataByHash(web3, contractAddress, computeArticleHash(current.record.owner, previous));
    if (Number(previousMetadata.record.blockNumber) < Number(blockNumber)) break;
    current = previousMetadata;
    previous--;
  }
  return Number(indexOfArticleByUser) - Number(current.record.indexOfArticleByUser);
};

const indexOfCommentForBlockForArticle = async (web3, contractAddress, metadata) => {
  const {
    blockNumber
  } = metadata.commentRecord;
  let current = metadata;
  let previous = Number(blockNumber) - 1;
  while (previous > 0) {
    const previousMetadata = await getCommentRecord(web3, contractAddress, metadata.articleHash, metadata.index);
    if (Number(previousMetadata.blockNumber) < Number(blockNumber)) break;
    current = previousMetadata;
    previous--;
  }
  return Number(metadata.index) - Number(previous);
};

const getComment = async (web3, contractAddress, articleHash, index) => {
  const record = await getCommentRecord(web3, contractAddress, articleHash, index);
  if (!Number(record.blockNumber)) return null;
  return fetchCompleteComment(web3, contractAddress, {
    articleHash,
    index,
    commentRecord: record
  });
};

const fetchCompleteArticle = async (web3, contractAddress, {
  record,
  entryRecord
}) => {
  const block = await web3.eth.getBlock(record.blockNumber, true);
  const candidates = block.transactions.filter(({
    from,
    to,
    input
  }) => from.toLowerCase() === record.owner.toLowerCase() && to && to.toLowerCase() === contractAddress.toLowerCase() && input.substr(0, 10) === PUBLISH_ARTICLE_SIGNATURE);
  const tx = candidates.length > 1 ? candidates[await indexOfArticleForBlockForUser(web3, contractAddress, {
    record,
    entryRecord
  })] : candidates[0];
  const [ _, packed ] = decodeParameters(['string', 'bytes'], tx.input.substr(10));
  return Object.assign({
    article: unpackArticle(packed)
  }, record, entryRecord);
};

const fetchCompleteComment = async (web3, contractAddress, {
  articleHash,
  index,
  commentRecord
}) => {
  const block = await web3.eth.getBlock(commentRecord.blockNumber, true);
  const candidates = block.transactions.filter(({
    to,
    input
  }) => {
    if (input.substr(0, 10) === PUBLISH_COMMENT_SIGNATURE && to && to.toLowerCase() === contractAddress.toLowerCase()) return true;
  }).map((tx) => {
    const params = decodeParameters(['bytes32', 'bytes'], tx.input.substr(10));
    return {
      tx,
      params
    };
  }).filter(({
    params: [ txArticleHash ]
  }) => articleHash === txArticleHash);
  const picked = candidates.length > 1 ? candidates[await indexOfCommentForBlockForArticle(web3, contractAddress, {
    commentRecord,
    index,
    articleHash
  })] : candidates[0];
  return Object.assign({
    comment: unpackComment(picked.params[1])
  }, commentRecord);
};
  
Object.assign(module.exports, {
  publishArticle,
  publishComment,
  unpackArticle,
  computeArticleHash,
  getComment,
  getArticleByHash,
  getArticleByUserAndNonce,
  getArticleByIndex,
  getArticleByPermalink
});
