'use strict';

const fs = require('fs-extra');
const path = require('path');
const easySolc = require('./easy-solc');

module.exports = async () => {
  const src = await fs.readFile(path.join(__dirname, '..', 'contracts', 'ETHStreetJournal.sol'), 'utf8');
  try {
    return easySolc('ETHStreetJournal', src);
  } catch (e) {
    e.errors.forEach((v) => console.error(v.formattedMessage));
    throw e;
  }
};
