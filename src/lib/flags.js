'use strict';

const encodeFlags = (useMarkdown, isPrivate, allowComments) => Number(Boolean(useMarkdown)) | Number(Boolean(isPrivate)) << 1 | Number(Boolean(allowComments)) << 2;

const decodeFlags = (data) => {
  const asNumber = Number(data);
  return {
    useMarkdown: Boolean(asNumber & 0x1),
    isPrivate: Boolean((asNumber & 0x2) >>> 1),
    allowComments: Boolean((asNumber & 0x4) >>> 2)
  };
};

Object.assign(module.exports, {
  encodeFlags,
  decodeFlags
});
