pragma solidity >= 0.5.0;

library SafeMath32 {
    function add(uint32 a, uint32 b) internal pure returns (uint32) {
        uint32 c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }
}

contract ETHStreetJournal {
  using SafeMath32 for uint32;
  struct Article {
    uint32 blockNumber;
    uint32 indexOfArticleByUser;
    uint32 globalArticleNumber;
    address owner;
  } 
  struct Comment {
    uint32 blockNumber;
    uint32 upvotes;
    uint32 downvotes;
    address owner;
    mapping (address => bool) voted;
  }
  struct ArticleEntry {
    uint256 index;
    uint32 downvotes;
    uint32 upvotes;
    uint32 commentCount; 
    string permalink;
    mapping (address => bool) voted;
  }
  struct AdditionalPage {
    uint32 blockNumber;
  }
  struct UserData {
    uint256 totalArticles;
    string username;
  }
  event ArticlePublished(address indexed author, uint32 indexed totalArticles);
  event CommentPublished(bytes32 indexed articleHash, uint32 indexed commentIndex);
  mapping (bytes32 => Comment) public comments;
  mapping (address => UserData) public userData;
  mapping (bytes32 => ArticleEntry) public idToEntry;
  mapping (bytes32 => AdditionalPage) public additionalPages;
  mapping (bytes32 => uint256) public permaLinkToIndex;
  Article[] public articles;
  function publishAdditionalPage(bytes32 articleHash) external {
    require(msg.sender == articles[idToEntry[articleHash].index].owner);
    bytes32 pageHash = keccak256(abi.encodePacked(articleHash));
    while (additionalPages[pageHash].blockNumber != 0) {
      pageHash = keccak256(abi.encodePacked(pageHash));
    }
    additionalPages[pageHash] = AdditionalPage({
      blockNumber: uint32(block.number)
    });
  }
  function publishArticle(string calldata permalink, bytes calldata /* payload */) external {
    uint32 totalArticles = uint32(userData[msg.sender].totalArticles);
    uint32 globalArticleNumber = uint32(articles.length);
    articles.push(Article({
      blockNumber: uint32(block.number),
      indexOfArticleByUser: totalArticles,
      globalArticleNumber: globalArticleNumber,
      owner: msg.sender
    }));
    idToEntry[keccak256(abi.encodePacked(msg.sender, totalArticles))] = ArticleEntry({
      index: globalArticleNumber,
      downvotes: uint32(0),
      upvotes: uint32(0),
      commentCount: uint32(0),
      permalink: permalink
    });
    if (bytes(permalink).length != 0) permaLinkToIndex[keccak256(abi.encodePacked(permalink))] = globalArticleNumber;
    userData[msg.sender].totalArticles = totalArticles.add(1);
    emit ArticlePublished(msg.sender, totalArticles);
  }
  function setUsername(string calldata username) external {
    userData[msg.sender].username = username;
  }
  function publishComment(bytes32 articleHash, bytes calldata /* payload */) external {
    ArticleEntry storage entry = idToEntry[articleHash];
    comments[keccak256(abi.encodePacked(articleHash, entry.commentCount))] = Comment({
      blockNumber: uint32(block.number),
      upvotes: uint32(0),
      downvotes: uint32(0),
      owner: msg.sender
    });
    emit CommentPublished(articleHash, entry.commentCount);
    entry.commentCount = entry.commentCount.add(1);
  }
  function upvoteComment(bytes32 commentHash) external {
    Comment storage comment = comments[commentHash];
    require(!comment.voted[msg.sender]);
    comment.voted[msg.sender] = true;
    comment.upvotes = comment.upvotes.add(1);
  }
  function downvoteComment(bytes32 commentHash) external {
    Comment storage comment = comments[commentHash];
    require(!comment.voted[msg.sender]);
    comment.voted[msg.sender] = true;
    comment.downvotes = comment.upvotes.add(1);
  }
  function upvoteArticle(bytes32 articleHash) external {
    ArticleEntry storage entry = idToEntry[articleHash];
    require(!entry.voted[msg.sender]);
    entry.voted[msg.sender] = true;
    entry.upvotes = entry.upvotes.add(1);
  }
  function downvoteArticle(bytes32 articleHash) external {
    ArticleEntry storage entry = idToEntry[articleHash];
    require(!entry.voted[msg.sender]);
    entry.voted[msg.sender] = true;
    entry.downvotes = entry.upvotes.add(1);
  }
}
